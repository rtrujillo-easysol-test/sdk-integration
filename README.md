# rtrujillo DID SDK integration

Integration of the DID SDK v7.0.0 following the 7.0.x guide/document

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

This projects requires a local DetectID server with version number 7.1.0, please refer to the en_MN_DID_InstallationManual.pdf document for installation and setup instructions.

A physical android device is required to run some of the SDK features.

### Project description

The DID demo Android project was develop targeting the latest android sdk version available at the time of writing of this document and it was written in Kotlin.

## Findings

- Specify in the SDK documentation that the Legacy ServerKey of the Firebase app should be copied to the Project identifier field in: Management Console->Channels->{{channel-id}}->Configuration->Push->Project identifier

- In the Face Authentication section (page 80) the line 

```
faceRegistrationViewProperties.FACE_SUCCESS_MESSAGE_PROGRESS_BAR_COLOR = "#FFFFF"; 
```NIZED_TOUCH_SATE = "Fingerprint notrec

throws the following exception

```
java.lang.IllegalArgumentException: invalid color#FFFFF
```

- In The Connection protection – Certificate Pinning section (page 106) the line

```
fingerprintTransactionViewProperties.NOT_RECOGNIZED_TOUCH_SATE = "Fingerprint not recognized";
```

has a typo. Change NOT_RECOGNIZED_TOUCH_SATE for por NOT_RECOGNIZED_TOUCH_STATE

- It would be ideal to provide a working sample (Kotlin and Java) with the documentation, so it can be used to copy code snippets from.

## Authors

* **rtrujillio** - *Initial work* - [rtrujillo](rtrujillo@easysol.net)
