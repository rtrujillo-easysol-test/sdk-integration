package com.example.rtrujillo.sdktest.presention.view

import android.app.AlertDialog
import android.content.Context
import android.os.AsyncTask
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.view.LayoutInflater
import android.widget.Button
import android.widget.Toast
import com.example.rtrujillo.sdktest.R
import net.easysol.did.DetectID
import java.lang.ref.WeakReference


class VoiceRegistrationActivity : AppCompatActivity() {

    private lateinit var context: Context
    private var isRegistrationVoiceRecording = false
    private lateinit var registrationDialog: AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.voice_registration_layout)

        context = applicationContext

        registrationSetup()
    }

    private fun registrationSetup() {
        DetectID.sdk(context).VOICE_API.setVoiceRegistrationReceivedListener { Log.i(javaClass.simpleName, "Notification received") }
        DetectID.sdk(context).VOICE_API.setVoiceRegistrationOpenListener { registrationVoiceProcess() }
        DetectID.sdk(context).VOICE_API.setVoiceRegistrationServerResponseListener { s ->
            val message = if ("1020" == s) "Voice successfully registered" else "Voice registration fail"
            showRegistrationMessage(message)
        }
    }

    private fun showRegistrationMessage(message: String) {
        runOnUiThread { Toast.makeText(context, message, Toast.LENGTH_SHORT).show() }
    }

    private fun registrationVoiceProcess() {
        if (!ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.RECORD_AUDIO)) {
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.RECORD_AUDIO), 100)
        }
        DetectID.sdk(context).VOICE_API.clearRegistrationVoiceSamples()
        val voiceRegistrationView = LayoutInflater.from(context).inflate(R.layout.voice_registration_layout, null)
        val button = voiceRegistrationView.findViewById<Button>(R.id.btnVoiceRegistration)
        button.setOnClickListener {
            if (!isRegistrationVoiceRecording) {
                isRegistrationVoiceRecording = DetectID.sdk(context).VOICE_API.startVoiceRegistrationRecording()
                button.text = "stop"
            } else {
                isRegistrationVoiceRecording = false
                DetectID.sdk(context).VOICE_API.stopVoiceRegistrationRecording()
                if (DetectID.sdk(context).VOICE_API.storedRegistrationVoiceSamplesCount < 3) {
                    button.text = "start"
                } else {
                    SendVoiceRegistrationTask(context).execute()
                    registrationDialog.dismiss()
                }
            }
            val builder = AlertDialog.Builder(this)
            registrationDialog = builder.create()
            registrationDialog.setView(voiceRegistrationView)
            registrationDialog.setCancelable(false)
            registrationDialog.show()
        }
    }

    private class SendVoiceRegistrationTask(context: Context) : AsyncTask<Void, Void, Void>() {

        private val context: WeakReference<Context> = WeakReference(context)

        override fun doInBackground(vararg urls: Void): Void? {
            try {
                DetectID.sdk(context.get()).VOICE_API.sendVoiceRegistrationAction()
            } catch (ex: Exception) {
                Log.i(javaClass.simpleName, ex.message)
            }
            return null
        }
    }
}
