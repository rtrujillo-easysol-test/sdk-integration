package com.example.rtrujillo.sdktest.presention.view

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.Gravity
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Toast
import com.example.rtrujillo.sdktest.R
import net.easysol.did.DetectID

class RegistrationActivity : AppCompatActivity() {

    private lateinit var context: Context

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)

        context = applicationContext

        initSdkListener()
        registerAccount()
    }

    private fun registerAccount() {
        val builder = AlertDialog.Builder(this)
        val input = EditText(this)
        val linearLayout = LinearLayout(context)
        linearLayout.orientation = LinearLayout.VERTICAL
        linearLayout.gravity = Gravity.CENTER
        linearLayout.addView(input)
        linearLayout.setPadding(16, 0, 16, 0)
        builder.setTitle("Device Registration")
        builder.setMessage("Enter your registration code.")
        builder.setView(linearLayout)
        builder.setPositiveButton("Accept") { _, _ ->
            DetectID.sdk(this@RegistrationActivity).deviceRegistrationByCode(input.text.toString())
        }
        builder.setNegativeButton("Cancel") { _, _ -> }
        builder.create()
        val dialog = builder.create()
        dialog.show()
    }

    private fun initSdkListener() {
        DetectID.sdk(context).setDeviceRegistrationServerResponseListener { response ->
            val message = if (response == "1020") "Device successfully registered" else "Device registration failed"
            showRegistrationMessage(message)
        }
    }

    private fun showRegistrationMessage(message: String) {
        runOnUiThread {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
        }
    }
}
