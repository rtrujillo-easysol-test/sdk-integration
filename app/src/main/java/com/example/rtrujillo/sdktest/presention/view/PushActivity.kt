package com.example.rtrujillo.sdktest.presention.view

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import com.example.rtrujillo.sdktest.R
import net.easysol.did.DetectID
import net.easysol.did.common.transaction.TransactionInfo
import net.easysol.did.push_auth.transaction.PushTransactionViewProperties
import net.easysol.did.push_auth.transaction.listener.PushTransactionReceivedListener


class PushActivity : AppCompatActivity() {

    private lateinit var context: Context

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)

        context = applicationContext

        authenticationReceivedProcess()
        authenticationNotificationOpenProcess()

        val properties = PushTransactionViewProperties(context)
        properties.NOTIFICATION_ICON_RESOURCE = android.R.drawable.stat_sys_warning
        properties.CONFIRM = "Confirm"
        properties.DECLINE = "Decline"

        DetectID.sdk(context).PUSH_API.setPushTransactionServerResponseListener { s ->
            val message = if ("1020" == s) "Authentication completed successfully" else "Authentication process fail"
            showAuthenticationMessage(message)
        }
    }

    private fun showAuthenticationMessage(message: String) {
        runOnUiThread { Toast.makeText(context, message, Toast.LENGTH_SHORT).show() }
    }

    private fun authenticationReceivedProcess() {
        DetectID.sdk(context).PUSH_API.setPushTransactionReceivedListener(object : PushTransactionReceivedListener {
            override fun onPushTransactionReceived(transactionInfo: TransactionInfo) {
                Log.i(javaClass.simpleName, "Notification received")
            }
        })
    }

    private fun authenticationNotificationOpenProcess() {
        DetectID.sdk(context).PUSH_API.setPushTransactionOpenListener { transactionInfo ->
            val builder = AlertDialog.Builder(this)
            builder.setTitle(buildTitle(transactionInfo))
            builder.setMessage(buildMessage(transactionInfo))
            builder.setPositiveButton("Confirm"
            ) { _, _ -> DetectID.sdk(context).PUSH_API.confirmPushTransactionAction(transactionInfo) }
            builder.setNegativeButton("Decline"
            ) { _, _ -> DetectID.sdk(context).PUSH_API.declinePushTransactionAction(transactionInfo) }
            builder.create()
            val dialog = builder.create()
            dialog.show()
        }
    }

    private fun buildMessage(transactionInfo: TransactionInfo?): String {
        return if (transactionInfo != null) {
            "message :" + transactionInfo.message
        } else {
            ""
        }
    }

    private fun buildTitle(transactionInfo: TransactionInfo?): String {
        return if (transactionInfo != null) {
            transactionInfo.account.organizationName + "\n"
        } else {
            ""
        }
    }
}
