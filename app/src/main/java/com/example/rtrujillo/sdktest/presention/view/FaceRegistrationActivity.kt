package com.example.rtrujillo.sdktest.presention.view

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import com.example.rtrujillo.sdktest.R
import net.easysol.did.DetectID
import net.easysol.did.face_auth.registration.FaceRegistrationViewProperties


class FaceRegistrationActivity : AppCompatActivity() {

    private lateinit var context: Context

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)

        context = applicationContext

        uiConfiguration()
        registrationSetup()
    }

    private fun registrationSetup() {
        DetectID.sdk(context).FACE_API.setFaceRegistrationReceivedListener { Log.i(javaClass.simpleName, "Notification received") }
        DetectID.sdk(context).FACE_API.setFaceRegistrationOpenListener { Log.i(javaClass.simpleName, "Notification opened") }
        DetectID.sdk(context).FACE_API.setFaceRegistrationServerResponseListener { s ->
            val message = if ("1020" == s) "Face successfully registered" else "Face registration fail"
            showRegistrationMessage(message)
        }
    }

    private fun uiConfiguration() {
        val faceRegistrationViewProperties = FaceRegistrationViewProperties(context)
        /** Errors  */
        faceRegistrationViewProperties.FACE_CAMERA_PERMISSION_ERROR = "The app doesn’t have the necessary permissions. Please change the settings of the device"
        faceRegistrationViewProperties.FACE_LIVENESS_UNSUCCESS = "The user’s face couldn’t be recognized"
        /** SelfID view  */
        faceRegistrationViewProperties.FACE_TIPS_TOO_CLOSE_MESSAGE_TEXT = "too close"
        faceRegistrationViewProperties.FACE_TIPS_TOO_CLOSE_MESSAGE_TEXT_SIZE = 20
        faceRegistrationViewProperties.FACE_TIPS_EXIT_MESSAGE_TEXT = "Do you want to exit?"
        faceRegistrationViewProperties.FACE_TIPS_EXIT_MESSAGE_TEXT_SIZE = 20
        faceRegistrationViewProperties.FACE_TIPS_MESSAGE_COLOR = "#FFCC22"
        /** Successful registration notification  */
        faceRegistrationViewProperties.FACE_SUCCESS_MESSAGE_TEXT = "Your message"
        faceRegistrationViewProperties.FACE_SUCCESS_MESSAGE_TEXT_SIZE = 20
        faceRegistrationViewProperties.FACE_SUCCESS_MESSAGE_PROGRESS_BAR_COLOR = "#FFFFFF"
        /** liveness gestures view  */
        faceRegistrationViewProperties.GESTURE_PROGRESS_BAR_COLOR = "#FFFFFF"
        faceRegistrationViewProperties.GESTURE_TEXT_SIZE = 20
        faceRegistrationViewProperties.FACE_LIVENESS_SHAPE_BACKGROUND_COLOR = "#66FFFFFF"
        faceRegistrationViewProperties.GESTURE_NEUTRAL_TEXT = "Stay still"
        faceRegistrationViewProperties.GESTURE_SIDE_TO_SIDE_TEXT = "Turn your head"
        faceRegistrationViewProperties.GESTURE_COLOR = "#FFCC22"
        faceRegistrationViewProperties.GESTURE_SMILE_TEXT = "Smile"
        faceRegistrationViewProperties.GESTURE_BLINK_TEXT = "Blink"
        /**Server Response Codes */
        faceRegistrationViewProperties.SERVER_RESPONSE_CODE_99 = "99"
        faceRegistrationViewProperties.SERVER_RESPONSE_CODE_98 = "98"
        faceRegistrationViewProperties.SERVER_RESPONSE_CODE_1002 = "1002"
        faceRegistrationViewProperties.SERVER_RESPONSE_CODE_1011 = "1011"
        faceRegistrationViewProperties.SERVER_RESPONSE_CODE_1012 = "1012"
        faceRegistrationViewProperties.SERVER_RESPONSE_CODE_1014 = "1014"
        faceRegistrationViewProperties.SERVER_RESPONSE_CODE_1020 = "1020"
        /**Server Response */
        faceRegistrationViewProperties.SERVER_RESPONSE_DIALOG_OK = "OK"
        faceRegistrationViewProperties.SERVER_RESPONSE_DIALOG_RETRY = "Try again"
        faceRegistrationViewProperties.SERVER_RESPONSE_DIALOG_TITLE = "Message"
        faceRegistrationViewProperties.SERVER_RESPONSE_DIALOG_CANCEL = "Cancelar"
        DetectID.sdk(context).FACE_API.setFaceRegistrationViewProperties(faceRegistrationViewProperties)
    }

    private fun showRegistrationMessage(message: String) {
        runOnUiThread { Toast.makeText(context, message, Toast.LENGTH_SHORT).show() }
    }
}
