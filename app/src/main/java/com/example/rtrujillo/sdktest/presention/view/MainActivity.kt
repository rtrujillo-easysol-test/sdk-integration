package com.example.rtrujillo.sdktest.presention.view

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.appcompat.app.AppCompatActivity
import android.widget.Button
import android.widget.Toast
import com.example.rtrujillo.sdktest.R
import com.example.rtrujillo.sdktest.databinding.ActivityMainBinding
import com.example.rtrujillo.sdktest.presention.interfaces.MainViewMvvm

class MainActivity : AppCompatActivity(), MainViewMvvm.View {

    private lateinit var context: Context
    private val REQUEST_CODE_CAMERA = 98
    private val REQUEST_CODE_RECORD_AUDIO = 99

    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: MainViewMvvm.ViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        context = applicationContext

        initButtonListeners()
        checkPermissions()
    }

    private fun checkPermissions() {
        if (hasNoPermissionFor(Manifest.permission.CAMERA)) makePermissionRequest(Manifest.permission.CAMERA, REQUEST_CODE_CAMERA)
        if (hasNoPermissionFor(Manifest.permission.RECORD_AUDIO)) makePermissionRequest(Manifest.permission.RECORD_AUDIO, REQUEST_CODE_RECORD_AUDIO)
    }

    private fun makePermissionRequest(permissionType: String, requestCode: Int) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, permissionType)) {
            alert("$permissionType permissions required")
        } else {
            ActivityCompat.requestPermissions(this, arrayOf(permissionType), requestCode)
        }
    }

    private fun alert(alertMessage: String) {
        runOnUiThread { Toast.makeText(this, alertMessage, Toast.LENGTH_SHORT).show() }
    }

    private fun hasNoPermissionFor(permissionType: String): Boolean {
        return ContextCompat.checkSelfPermission(this, permissionType) != PackageManager.PERMISSION_GRANTED
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_CODE_CAMERA -> onPermissions(Manifest.permission.CAMERA, grantResults)
            REQUEST_CODE_RECORD_AUDIO -> onPermissions(Manifest.permission.RECORD_AUDIO, grantResults)
        }
    }

    private fun onPermissions(permissionName: String, grantResults: IntArray) {
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            runOnUiThread { Toast.makeText(context, "$permissionName permissions granted", Toast.LENGTH_SHORT).show() }
        } else {
            runOnUiThread { Toast.makeText(context, "$permissionName permissions denied", Toast.LENGTH_SHORT).show() }
        }
    }

    private fun initButtonListeners() {
        val codeRegistrationButton = findViewById<Button>(R.id.code_registration)
        val qrRegistrationButton = findViewById<Button>(R.id.qr_registration)
        val otpButton = findViewById<Button>(R.id.otp)
        val qrAuthentication = findViewById<Button>(R.id.qr_authentication)
        val pushAuthentication = findViewById<Button>(R.id.push_authentication)
        val voiceRegistration = findViewById<Button>(R.id.voice_registration)
        val voiceAuthentication = findViewById<Button>(R.id.voice_authentication)
        val faceRegistration = findViewById<Button>(R.id.face_registration)
        val faceAuthentication = findViewById<Button>(R.id.face_authentication)
        val transactionInbox = findViewById<Button>(R.id.transaction_inbox)

        codeRegistrationButton.setOnClickListener {
            val intent = Intent(this, RegistrationActivity::class.java)
            startActivity(intent)
        }

        qrRegistrationButton.setOnClickListener {
            val intent = Intent(this, QrRegistrationActivity::class.java)
            startActivity(intent)
        }
        otpButton.setOnClickListener {
            val intent = Intent(this, OtpActivity::class.java)
            startActivity(intent)
        }

        qrAuthentication.setOnClickListener {
            val intent = Intent(this, QrValidationActivity::class.java)
            startActivity(intent)
        }

        pushAuthentication.setOnClickListener {
            val intent = Intent(this, PushActivity::class.java)
            startActivity(intent)
        }

        voiceRegistration.setOnClickListener {
            val intent = Intent(this, VoiceRegistrationActivity::class.java)
            startActivity(intent)
        }

        voiceAuthentication.setOnClickListener {
            val intent = Intent(this, VoiceValidationActivity::class.java)
            startActivity(intent)
        }

        faceRegistration.setOnClickListener {
            val intent = Intent(this, FaceRegistrationActivity::class.java)
            startActivity(intent)
        }

        faceAuthentication.setOnClickListener {
            val intent = Intent(this, FaceValidationActivity::class.java)
            startActivity(intent)
        }

        transactionInbox.setOnClickListener {
            val intent = Intent(this, TransactionInboxActivity::class.java)
            startActivity(intent)
        }
    }
}
