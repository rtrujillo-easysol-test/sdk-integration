package com.example.rtrujillo.sdktest

import android.app.Application
import android.util.Log
import net.easysol.did.DetectID
import net.easysol.did.common.fingerprint.FingerprintRegistrationViewProperties
import net.easysol.did.common.fingerprint.FingerprintTransactionViewProperties
import net.easysol.did.common.listener.SdkSupportVersionResponseListener
import net.easysol.did.common.model.contract.InitParams
import net.easysol.did.libs.DIDAppLifeCycleHandler

class App : Application(), SdkSupportVersionResponseListener {

    private var dIDAppLifeCycleHandler: DIDAppLifeCycleHandler = DIDAppLifeCycleHandler()

    override fun onCreate() {
        super.onCreate()

        initDID()

        registerActivityLifecycleCallbacks(dIDAppLifeCycleHandler)
    }

    private fun initDID() {
        val serverHost: String = getString(R.string.server_host)
        val serverPort: String = getString(R.string.server_port)
        val initParams: InitParams = InitParams.InitParamsBuilder()
                .addDidUrl("http://$serverHost:$serverPort/detect/public/registration/mobileServices.htm?code=")
                .addSdkSupportVersionResponseListener(this)
                .build()

        DetectID.sdk(this).enableSecureCertificateValidationProtocol(true)
        DetectID.sdk(this).PUSH_API.enablePushAlertDefaultDialog(true)
        DetectID.sdk(this).PUSH_API.enablePushTransactionDefaultDialog(true)
        DetectID.sdk(this).PUSH_API.enablePushTransactionServerResponseAlerts(true)

        DetectID.sdk(this).QR_API.enableQRCodeTransactionDefaultDialog(true)
        DetectID.sdk(this).QR_API.enableQRCodeTransactionServerResponseAlerts(true)

        DetectID.sdk(this).VOICE_API.enableVoiceRegistrationDefaultDialog(true)
        DetectID.sdk(this).VOICE_API.enableVoiceRegistrationServerResponseAlerts(true)
        DetectID.sdk(this).VOICE_API.enableVoiceTransactionDefaultDialog(true)
        DetectID.sdk(this).VOICE_API.enableVoiceTransactionServerResponseAlerts(true)
        DetectID.sdk(this).VOICE_API.enableVoiceRegistrationDefaultDialog(true)

        DetectID.sdk(this).FACE_API.enableFaceRegistrationServerResponseAlerts(true)
        DetectID.sdk(this).FACE_API.enableFaceTransactionServerResponseAlerts(true)
        DetectID.sdk(this).FACE_API.enableFaceRegistrationAutomaticOpen(false)
        DetectID.sdk(this).FACE_API.enableFaceTransactionAutomaticOpen(false)

        DetectID.sdk(this).setFingerprintTransactionViewProperties(getFingerprintTransactionViewProperties())
        DetectID.sdk(this).setFingerprintRegistrationViewProperties(getFingerprintRegistrationViewProperties())

        DetectID.sdk(this).INBOX_API.enableBadgeNumber(true)

        DetectID.sdk(applicationContext).initDIDServerWithParams(initParams)

        DetectID.sdk(this).isSdkVersionSupported
    }

    private fun getFingerprintRegistrationViewProperties(): FingerprintRegistrationViewProperties? {
        val fingerprintRegistrationViewProperties = FingerprintRegistrationViewProperties(this)

        fingerprintRegistrationViewProperties.TITLE = "Fingerprint registration"
        fingerprintRegistrationViewProperties.MESSAGE = "There are not fingerprint registered, please use settings button to registered a fingerprint"
        fingerprintRegistrationViewProperties.SETTINGS = "Settings"
        fingerprintRegistrationViewProperties.CANCEL = "Cancel"

        return fingerprintRegistrationViewProperties
    }

    private fun getFingerprintTransactionViewProperties(): FingerprintTransactionViewProperties? {
        val fingerprintTransactionViewProperties = FingerprintTransactionViewProperties(this)

        fingerprintTransactionViewProperties.ACTIVE_PUSH_TRANSACTION = true
        fingerprintTransactionViewProperties.ACTIVE_QR_CODE_TRANSACTION = true
        fingerprintTransactionViewProperties.FORCE_FINGERPRINT_REGISTRATION = true
        fingerprintTransactionViewProperties.ALLOW_SECOND_PASSWORD = true
        fingerprintTransactionViewProperties.TITLE = "Fingerprint validation"
        fingerprintTransactionViewProperties.MESSAGE = "This operation need to be authorize using your fingerprint"
        fingerprintTransactionViewProperties.CANCEL = "Cancel"
        fingerprintTransactionViewProperties.ASK_TOUCH_STATE = "Touch the sensor please."
        fingerprintTransactionViewProperties.SUCCESS_TOUCH_STATE = "Success Fingerprint validation"

        fingerprintTransactionViewProperties.NOT_RECOGNIZED_TOUCH_STATE = "Fingerprint notrecognized"
        fingerprintTransactionViewProperties.NOT_DETECTED_TOUCH_STATE = "Fingerprint not detected"
        fingerprintTransactionViewProperties.LOCKOUT_TOUCH_STATE = "Lockout fingerprint"
        fingerprintTransactionViewProperties.OPTIONAL_PASSCODE = "Use device password"
        fingerprintTransactionViewProperties.TRANSACTION_NOT_AUTHORIZED_BY_FINGERPRINT = "The transaction is not authorized by fingerprint"

        return fingerprintTransactionViewProperties
    }

    override fun onSuccessResponse(p0: Boolean) {
        Log.i(javaClass.simpleName, "onSuccessResponse")
    }

    override fun onFailedResponse() {
        Log.i(javaClass.simpleName, "onFailedResponse")
    }
}
