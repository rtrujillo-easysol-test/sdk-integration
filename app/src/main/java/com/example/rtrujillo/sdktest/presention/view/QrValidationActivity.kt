package com.example.rtrujillo.sdktest.presention.view

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import android.widget.TextView
import android.widget.Toast
import com.example.rtrujillo.sdktest.R
import net.easysol.did.DetectID
import net.easysol.did.common.transaction.TransactionInfo


class QrValidationActivity : AppCompatActivity() {

    private lateinit var context: Context

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)

        context = applicationContext

        displayCameraForAuthentication()
        authenticationReadProcess()

        DetectID.sdk(context).QR_API.setQRCodeTransactionServerResponseListener { s ->
            val message = if ("1020" == s) "Authentication completed successfully" else "Authentication process fail"
            showAuthenticationMessage(message)
        }

        val title = findViewById<TextView>(R.id.title)
        title.text = javaClass.simpleName
    }

    private fun displayCameraForAuthentication() {
        if (DetectID.sdk(context).existAccounts()) {
            val accounts = DetectID.sdk(context).accounts
            val account = accounts[0]
            DetectID.sdk(context).QR_API.scanQRCodeTransactionCode(account)
        }
    }

    private fun showAuthenticationMessage(message: String) {
        runOnUiThread { Toast.makeText(context, message, Toast.LENGTH_SHORT).show() }
    }

    private fun authenticationReadProcess() {
        DetectID.sdk(context).QR_API.setQRCodeScanTransactionListener { transactionInfo ->
            val builder = AlertDialog.Builder(this)
            builder.setTitle(buildTitle(transactionInfo))
            builder.setMessage(buildMessage(transactionInfo))
            builder.setPositiveButton("Confirm"
            ) { _, _ -> DetectID.sdk(context).QR_API.confirmQRCodeTransactionAction(transactionInfo) }
            builder.setNegativeButton("Decline"
            ) { _, _ -> DetectID.sdk(context).QR_API.declineQRCodeTransactionAction(transactionInfo) }
            Handler(Looper.getMainLooper()).post {
                val dialog = builder.create()
                dialog.show()
            }
        }
    }

    private fun buildTitle(transactionInfo: TransactionInfo?): String {
        return if (transactionInfo != null) {
            transactionInfo.account.organizationName + "\n"
        } else {
            ""
        }
    }

    private fun buildMessage(transactionInfo: TransactionInfo?): String {
        return if (transactionInfo != null) {
            val message = StringBuilder()
            message.append("message :")
            message.append(transactionInfo.message)
            message.append("\n")
            message.append("offline :")
            message.append(transactionInfo.transactionOfflineCode)
            message.toString()
        } else {
            ""
        }
    }
}
