package com.example.rtrujillo.sdktest.presention.view

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import com.example.rtrujillo.sdktest.R
import net.easysol.did.DetectID
import net.easysol.did.common.transaction.TransactionInfo
import net.easysol.did.push_auth.alert.PushAlertViewProperties
import net.easysol.did.push_auth.alert.listener.PushAlertReceivedListener


class PushAlertActivity : AppCompatActivity() {

    private lateinit var context: Context

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)

        context = applicationContext

        alertReceivedProcess()
        alertNotificationOpenProcess()

        val properties = PushAlertViewProperties(context)
        properties.NOTIFICATION_ICON_RESOURCE = android.R.drawable.stat_sys_warning
        DetectID.sdk(context).PUSH_API.setPushAlertViewProperties(properties)

        DetectID.sdk(context).PUSH_API.setPushAlertReceivedListener(object : PushAlertReceivedListener {
            override fun onPushAlertReceived(transactionInfo: TransactionInfo) {
                Log.i(javaClass.simpleName, "Notification received")
            }
        })
    }

    private fun alertReceivedProcess() {
        runOnUiThread { Toast.makeText(context, "alertReceivedProcess", Toast.LENGTH_SHORT).show() }
    }

    private fun alertNotificationOpenProcess() {
        /**Push alert open listener implementation  */
        DetectID.sdk(context).PUSH_API.setPushAlertOpenListener { transactionInfo ->
            val builder = AlertDialog.Builder(this)
            builder.setTitle(buildTitle(transactionInfo))
            builder.setMessage(buildMessage(transactionInfo))
            builder.setPositiveButton("Approve"
            ) { dialog, which -> DetectID.sdk(context).PUSH_API.approvePushAlertAction(transactionInfo) }
            val dialog = builder.create()
            dialog.show()
        }
    }

    private fun buildMessage(transactionInfo: TransactionInfo?): String {
        return if (transactionInfo != null) {
            "message :" + transactionInfo.message
        } else {
            ""
        }
    }

    private fun buildTitle(transactionInfo: TransactionInfo?): String {
        return if (transactionInfo != null) {
            transactionInfo.account.organizationName + "\n"
        } else {
            ""
        }
    }
}
