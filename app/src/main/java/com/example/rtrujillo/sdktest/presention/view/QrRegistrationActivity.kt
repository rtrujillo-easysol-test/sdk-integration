package com.example.rtrujillo.sdktest.presention.view

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.widget.TextView
import android.widget.Toast
import com.example.rtrujillo.sdktest.R
import net.easysol.did.DetectID

class QrRegistrationActivity : AppCompatActivity() {

    private lateinit var context: Context

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)

        context = applicationContext

        initSdkListener()
        registerAccountWithQR()

        val title = findViewById<TextView>(R.id.title)
        title.text = javaClass.simpleName
    }

    private fun registerAccountWithQR() {
        DetectID.sdk(context).deviceRegistrationByQRCode()
    }

    private fun initSdkListener() {
        DetectID.sdk(context).setDeviceRegistrationServerResponseListener { response ->
            val message = if (response == "1020") "Device successfully registered" else "Device registration failed"
            showRegistrationMessage(message)
        }
    }

    private fun showRegistrationMessage(message: String) {
        runOnUiThread {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
        }
    }
}
