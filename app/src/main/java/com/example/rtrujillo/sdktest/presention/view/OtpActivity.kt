package com.example.rtrujillo.sdktest.presention.view

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.Toast
import com.example.rtrujillo.sdktest.R
import net.easysol.did.DetectID
import net.easysol.did.otp_auth.TokenViewProperties
import net.easysol.did.otp_auth.totp.TokenView

class OtpActivity : AppCompatActivity() {

    private lateinit var context: Context

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otp)

        context = applicationContext

        initSdkListener()

        setBusinessTokenUi()
        val tokenView = prepareTokenView()
        addTokenViewToTheActivity(tokenView)
    }

    private fun addTokenViewToTheActivity(tokenView: TokenView?) {
        if (tokenView != null) {
            val container = findViewById<ViewGroup>(R.id.tokenViewContainer)
            val containerParameters = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT)
            containerParameters.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE)
            container.addView(tokenView, containerParameters)
        } else {
            Log.i(javaClass.simpleName, "It is impossible to get the token view")
        }
    }

    private fun prepareTokenView(): TokenView? {
        var tokenView: TokenView? = null
        if (DetectID.sdk(context).existAccounts()) {
            val accounts = DetectID.sdk(context).accounts
            val account = accounts[0]
            tokenView = DetectID.sdk(context).OTP_API.getTokenView(account)
        }
        return tokenView
    }

    private fun setBusinessTokenUi() {
        val tokenUiProperties = TokenViewProperties(context)
        tokenUiProperties.FONT_COLOR = "#111111"
        tokenUiProperties.PROGRES_BAR_BACKGROUND_COLOR = "#eeeeee"
        tokenUiProperties.PROGRESS_BAR_COLOR = "#8bc34a"
        tokenUiProperties.PROGRESS_BAR_HEIGHT = 70
        tokenUiProperties.PROGRESS_BAR_ROUND_CORNERS = true
        DetectID.sdk(context).OTP_API.setTokenViewProperties(tokenUiProperties)
    }

    private fun initSdkListener() {
        DetectID.sdk(context).setDeviceRegistrationServerResponseListener { response ->
            val message = if (response == "1020") "Device successfully registered" else "Device registration failed"
            showRegistrationMessage(message)
        }
    }

    private fun showRegistrationMessage(message: String) {
        runOnUiThread {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
        }
    }
}
