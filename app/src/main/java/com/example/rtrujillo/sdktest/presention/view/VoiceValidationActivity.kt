package com.example.rtrujillo.sdktest.presention.view

import android.app.AlertDialog
import android.content.Context
import android.os.AsyncTask
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.view.LayoutInflater
import android.widget.Button
import android.widget.Toast
import com.example.rtrujillo.sdktest.R
import net.easysol.did.DetectID
import net.easysol.did.common.transaction.TransactionInfo
import net.easysol.did.voice_auth.transaction.listener.VoiceTransactionReceivedListener
import java.lang.ref.WeakReference

class VoiceValidationActivity : AppCompatActivity() {

    private lateinit var context: Context
    private var isAuthenticationVoiceRecording = false
    private lateinit var authenticationDialog: AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.voice_registration_layout)

        context = applicationContext

        authenticationSetup()
    }

    private fun authenticationSetup() {
        DetectID.sdk(context).VOICE_API.setVoiceTransactionReceivedListener(object : VoiceTransactionReceivedListener {
            override fun onVoiceTransactionReceivedListener(transactionInfo: TransactionInfo) {
                Log.i(javaClass.simpleName, "Notification received")
            }
        })

        DetectID.sdk(context).VOICE_API.setVoiceTransactionOpenListener { transactionInfo -> authenticationVoiceProcess(transactionInfo) }

        DetectID.sdk(context).VOICE_API.setVoiceTransactionServerResponseListener { s ->
            val message = if ("1020" == s) "Voice successfully authenticated" else "Voice authentication fail"
            showAuthenticationMessage(message)
        }
    }

    private fun showAuthenticationMessage(message: String) {
        runOnUiThread { Toast.makeText(context, message, Toast.LENGTH_SHORT).show() }
    }

    private fun authenticationVoiceProcess(transactionInfo: TransactionInfo) {
        DetectID.sdk(context).VOICE_API.clearTransactionVoiceSample()
        val voiceAuthenticationView = LayoutInflater.from(context).inflate(R.layout.voice_authentication_layout, null)
        val button = voiceAuthenticationView.findViewById<Button>(R.id.btnVoiceAuthentication)
        button.setOnClickListener {
            if (!isAuthenticationVoiceRecording) {
                isAuthenticationVoiceRecording = DetectID.sdk(context).VOICE_API.startVoiceTransactionRecording()
                button.setText(R.string.voice_dialog_stop_action)
            } else {
                isAuthenticationVoiceRecording = false
                DetectID.sdk(context).VOICE_API.stopVoiceTransactionRecording()
                SendVoiceAuthenticationTask(context, transactionInfo).execute()
                authenticationDialog.dismiss()
            }
        }
        val builder = AlertDialog.Builder(this)
        authenticationDialog = builder.create()
        authenticationDialog.setView(voiceAuthenticationView)
        authenticationDialog.setCancelable(false)
        authenticationDialog.show()
    }

    private class SendVoiceAuthenticationTask(context: Context, private val transactionInfo: TransactionInfo) : AsyncTask<Void, Void, Void>() {

        private var context: WeakReference<Context> = WeakReference(context)

        override fun doInBackground(vararg urls: Void): Void? {
            DetectID.sdk(context.get()).VOICE_API.sendVoiceTransactionAction(transactionInfo)
            return null
        }
    }
}
