package com.example.rtrujillo.sdktest.presention.view

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.example.rtrujillo.sdktest.R
import net.easysol.did.DetectID
import net.easysol.did.common.account.AccountController
import net.easysol.did.common.transaction.TransactionInfo
import net.easysol.did.inbox.listener.TransactionInfoListener


class TransactionInboxActivity : AppCompatActivity() {

    private lateinit var context: Context
    private val LOG_TAG = "PUSH INBOX"
    private val URL_PUSH_ALERT = "%s/detect/public/rest/mobileServices/transactions/alert/"
    private val URL_PUSH_AUTH = "%s/detect/public/rest/mobileServices/transactions/auth/"
    private val URL_PUSH_BIOMETRIC = "%s/detect/public/rest/biometrics/transactions/"

    private val mRecyclerView: androidx.recyclerview.widget.RecyclerView? = null
    private var mAdapter: androidx.recyclerview.widget.RecyclerView.Adapter<*>? = null
    private val mLayoutManager: androidx.recyclerview.widget.RecyclerView.LayoutManager? = null
    private lateinit var alertDialogInbox: AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)

        context = applicationContext

        displayPushAuthenticationInbox()
    }

    /**
     * On the recycler view, displays all the push authentications by account,
     * for this example, the inbox works with TransactionType.PUSH_AUTHENTICATION * you can configure different types like:
     * - PUSH_AUTHENTICATION
     * - PUSH_ALERT
     * - BIOMETRICS
     */
    private fun displayPushAuthenticationInbox() {
        /** validate if any accounts exist  */
        if (DetectID.sdk(context).existAccounts()) {
            /** get all the accounts registered before, this example works for single account  */
            val account = DetectID.sdk(context).accounts[0]
            /** get registration url by account */
            val accountUrl = AccountController.getInstance(context).getAccountURL(account)
            val parts = accountUrl.split("/detect/".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            val urlPushAlert = String.format(URL_PUSH_ALERT, parts[0])
            val urlPushAuth = String.format(URL_PUSH_AUTH, parts[0])
            val pushBiometric = String.format(URL_PUSH_BIOMETRIC, parts[0])
            /** setup transaction inbox implementation */
            DetectID.sdk(context).INBOX_API.setupTransactionInbox(urlPushAlert, urlPushAuth, pushBiometric)
            /** All transactions by type implementation */
            DetectID.sdk(context).INBOX_API.getAllTransactionsByType(account, TransactionInfo.TransactionType.PUSH_AUTHENTICATION, 1,
                    object : TransactionInfoListener {
                        override fun onResponseSuccessful(list: List<TransactionInfo>, totalPages: Int, totalRecords: Int) {
                            val viewInbox = LayoutInflater.from(context).inflate(R.layout.inbox_push_view, null)
                            val recyclerView = viewInbox.findViewById<androidx.recyclerview.widget.RecyclerView>(R.id.recyclerViewInbox)
                            val buttonPush = viewInbox.findViewById<Button>(R.id.btnCerrarPush)

                            buttonPush.setOnClickListener { alertDialogInbox.dismiss() }
                            recyclerView.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(context)
                            mAdapter = TransactionAdapter(list)
                            recyclerView.adapter = mAdapter

                            val builder = AlertDialog.Builder(this@TransactionInboxActivity)
                            builder.setView(viewInbox)
                            alertDialogInbox = builder.create()
                            alertDialogInbox.setCancelable(false)
                            alertDialogInbox.show()
                        }

                        override fun onResponseFail(s: String) {
                            Log.d(LOG_TAG, "Request fail : $s")
                        }
                    })
        }
    }

    private inner class TransactionAdapter(private val mDataset: List<TransactionInfo>) : androidx.recyclerview.widget.RecyclerView.Adapter<TransactionAdapter.ViewHolder>() {

        internal inner class ViewHolder(v: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(v) {
            // each data item is just a string in this case
            var mCardView: androidx.cardview.widget.CardView
            var mTextView: TextView

            init {
                mCardView = v.findViewById<View>(R.id.card_view_transaction) as androidx.cardview.widget.CardView
                mTextView = v.findViewById<View>(R.id.transaction_subject) as TextView
            }
        }

        // Creates new views (invoked by the layout manager)
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            // creates a new view
            val v = LayoutInflater.from(parent.context).inflate(R.layout.inbox_push_item, parent, false)
            // set the view's size, margins, paddings and layout parameters
            return ViewHolder(v)
        }

        // Replace the contents of a view (invoked by the layout manager)
        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.mTextView.text = mDataset[position].subject
            holder.mCardView.setOnClickListener { authenticationDisplayProcess(mDataset[position]) }
        }

        override fun getItemCount(): Int {
            return mDataset.size
        }
    }

    private fun authenticationDisplayProcess(transactionInfo: TransactionInfo) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(buildTitle(transactionInfo))
        builder.setMessage(buildMessage(transactionInfo))
        builder.setPositiveButton("Confirm"
        ) { dialog, which -> DetectID.sdk(context).PUSH_API.confirmPushTransactionAction(transactionInfo) }
        builder.setNegativeButton("Decline"
        ) { dialog, which -> DetectID.sdk(context).PUSH_API.declinePushTransactionAction(transactionInfo) }
        builder.create()
        val dialog = builder.create()
        dialog.show()
    }

    private fun buildTitle(transactionInfo: TransactionInfo?): String {
        if (transactionInfo != null) {
            val title = StringBuilder()
            title.append(transactionInfo.account.organizationName)
            title.append("\n")
            return title.toString()
        } else {
            return ""
        }
    }

    /**
     * build the massage showed on the authentication dialog
     *
     * @param transactionInfo object that represents the transaction * @return the message string value
     */
    private fun buildMessage(transactionInfo: TransactionInfo?): String {
        if (transactionInfo != null) {
            val message = StringBuilder()
            message.append("message :")
            message.append(transactionInfo.message)
            return message.toString()
        } else {
            return ""
        }
    }
}
